#include "rectangle.h"

#include "util.h"


struct int_rectangle int_rectangle_intersection (struct int_rectangle r1, struct int_rectangle r2)
{
  struct int_rectangle res;
  res.lo[0] = MAX(r1.lo[0], r2.lo[0]);
  res.lo[1] = MAX(r1.lo[1], r2.lo[1]);
  res.hi[0] = MIN(r1.hi[0], r2.hi[0]);
  res.hi[1] = MIN(r1.hi[1], r2.hi[1]);
  return res;
}
struct float_rectangle float_rectangle_intersection (struct float_rectangle r1, struct float_rectangle r2)
{
  struct float_rectangle res;
  res.lo[0] = MAX(r1.lo[0], r2.lo[0]);
  res.lo[1] = MAX(r1.lo[1], r2.lo[1]);
  res.hi[0] = MIN(r1.hi[0], r2.hi[0]);
  res.hi[1] = MIN(r1.hi[1], r2.hi[1]);
  return res;
}

float float_rectangle_get_size (struct float_rectangle r, int dimension)
{
  return r.hi[dimension] - r.lo[dimension];
}

int int_rectangle_get_size (struct int_rectangle r, int dimension)
{
  return r.hi[dimension] - r.lo[dimension];
}
