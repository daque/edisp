#pragma once

#include <SDL2/SDL.h>
#include "rectangle.h"

struct float_rectangle SDL_Rect_to_float_rectangle (SDL_Rect rect);
SDL_Rect float_rectangle_to_SDL_Rect (struct float_rectangle);
