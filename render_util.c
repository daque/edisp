#include "render_util.h"

#include "util.h"

SDL_Rect
get_minimum_cover (SDL_Rect source_rect,
		   int screen_width,
		   int screen_height)
{
  float factor_x = (float) source_rect.w / (float) screen_width;
  float factor_y = (float) source_rect.h / (float) screen_height;
  float factor = MAX(factor_x, factor_y);
  factor = MAX(factor, 1);
  float center_x = source_rect.x + source_rect.w * 0.5f;
  float center_y = source_rect.y + source_rect.h * 0.5f;
  SDL_Rect effective_rect;
  effective_rect.w = screen_width * factor;
  effective_rect.h = screen_height * factor;
  effective_rect.x = center_x - effective_rect.w / 2;
  effective_rect.y = center_y - effective_rect.h / 2;
  return effective_rect;
}

void
blit_keeping_proportions (SDL_Surface *source,
			  SDL_Rect source_rect,
			  SDL_Surface *screen)
{
  int screen_width = screen->w;
  int screen_height = screen->h;
  SDL_Rect effective_rect = get_minimum_cover (source_rect,
					       screen_width,
					       screen_height);
  SDL_BlitScaled (source, &effective_rect, screen, NULL);
}
