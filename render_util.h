#pragma once

#include <SDL2/SDL.h>

SDL_Rect get_minimum_cover (SDL_Rect to_cover, int w, int h);
void blit_keeping_proportions (SDL_Surface *source,
			       SDL_Rect source_rect,
			       SDL_Surface *screen);
