#include "rectangle_sdl_rect.h"

struct float_rectangle
SDL_Rect_to_float_rectangle (SDL_Rect rect)
{
  struct float_rectangle r;
  r.lo[0] = rect.x;
  r.lo[1] = rect.y;
  r.hi[0] = rect.x + rect.w;
  r.hi[1] = rect.y + rect.h;
  return r;
}

SDL_Rect
float_rectangle_to_SDL_Rect (struct float_rectangle r)
{
  SDL_Rect rect;
  rect.x = roundf (r.lo[0]);
  rect.y = roundf (r.lo[1]);
  rect.w = roundf (r.hi[0] - r.lo[0]);
  rect.h = roundf (r.hi[1] - r.lo[1]);
  return rect;
}
