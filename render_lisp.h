#include "lisp.h"

#define RENDER_PADDING 20

void
render_lisp_update_locally (struct lisp_value *value, int x, int y);

void
render_lisp_value (struct lisp_value *value, int x, int y);
