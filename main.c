#include "lisp.h"
#include "render_lisp.h"
#include "lisp_reader.h"
#include "font.h"
#include "render_util.h"
#include "rectangle.h"
#include "rectangle_sdl_rect.h"

#include <SDL2/SDL.h>

#include <stdio.h>

int
main (int argc, char* argv[])
{
  TTF_Init ();
  SDL_Init (SDL_INIT_EVERYTHING);
  font = TTF_OpenFont ("iosevka-regular.ttf", 20);
  reader_init_with_file ("test.scm");
  struct lisp_value *value = read_all ();
  lisp_value_print (value);
  putchar ('\n');
  render_lisp_update_locally (value, 0, 0);
  SDL_SaveBMP (value->rendered, "img.bmp");
  struct lisp_value *current = value;

  SDL_Window *window = SDL_CreateWindow ("test",
					 SDL_WINDOWPOS_UNDEFINED,
					 SDL_WINDOWPOS_UNDEFINED,
					 1300,
					 600,
					 0);
  SDL_Surface *screen = SDL_GetWindowSurface (window);
  SDL_Rect current_rect;
  int view_padding = 20;
  current_rect.x = current->render_x - view_padding;
  current_rect.y = current->render_y - view_padding;
  current_rect.w = current->rendered->w + 2 * view_padding;
  current_rect.h = current->rendered->h + 2 * view_padding;  
  SDL_Rect objective_view = get_minimum_cover (current_rect,
					       screen->w,
					       screen->h);
  struct float_rectangle current_view =
    SDL_Rect_to_float_rectangle (objective_view);
  for (;;)
    {
      SDL_FillRect (screen, NULL, SDL_MapRGB (screen->format,
					      60, 60, 60));
      current_rect.x = current->render_x - view_padding;
      current_rect.y = current->render_y - view_padding;
      current_rect.w = current->rendered->w + 2 * view_padding;
      current_rect.h = current->rendered->h + 2 * view_padding;
      objective_view = get_minimum_cover (current_rect,
					  screen->w,
					  screen->h);
      float factor = 0.02;
#define interpolate(from, to) from += ((to) - (from)) * factor
      interpolate(current_view.lo[0], objective_view.x);
      interpolate(current_view.lo[1], objective_view.y);
      interpolate(current_view.hi[0], objective_view.x + objective_view.w);
      interpolate(current_view.hi[1], objective_view.y + objective_view.h);
      SDL_Rect current_view_sdl_rect =
	float_rectangle_to_SDL_Rect (current_view);
      SDL_BlitScaled (value->rendered, &current_view_sdl_rect,
		      screen, NULL);
      SDL_UpdateWindowSurface (window);
      SDL_Event event;
      while (SDL_PollEvent (&event))
	{
	  switch (event.type)
	    {
	    case SDL_QUIT:
	      goto quit;
	    case SDL_KEYDOWN:
	      switch (event.key.keysym.scancode)
		{
		case SDL_SCANCODE_DOWN:
		  if (current->next) current = current->next;
		  break;
		case SDL_SCANCODE_UP:
		  if (current->previous) current = current->previous;
		  break;
		case SDL_SCANCODE_RIGHT:
		  if (current->type == TYPE_LIST && current->list.head)
		      current = current->list.head;
		  break;
		case SDL_SCANCODE_LEFT:
		  if (current->parent) current = current->parent;
		  break;
		}
	      break;
	    }
	}
    }
 quit:
  return 0;
}
