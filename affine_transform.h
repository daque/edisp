#pragma once
#include "rectangle.h"

struct affine_transform
{
  float factor, adder;
};

struct affine_transform_2d
{
  struct affine_transform t[2];
};

struct affine_transform linear_interpolation (float a, float b, float A, float B);
struct affine_transform_2d linear_interpolation_2d (struct float_rectangle a, struct float_rectangle b);
void apply_affine_transform (struct affine_transform t, float *x);
void apply_affine_transform_2d (struct affine_transform_2d t, float *x, float *y);
