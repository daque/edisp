#include "render_lisp.h"

#include "font.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <assert.h>

void
render_lisp_update_locally (struct lisp_value *value, int x, int y)
{
  
  if (value->rendered)
    SDL_FreeSurface (value->rendered);
  
  value->render_x = x;
  value->render_y = y;
  
  if (value->type == TYPE_LIST)
    {
      int children_max_width = 0;
      int children_height_sum = 0;
      int no_children = 0;
      
      SDL_Rect render_rect;
      render_rect.x = RENDER_PADDING;
      render_rect.y = RENDER_PADDING;      
      for (struct lisp_value *child = value->list.head;
	   child;
	   child = child->next)
	{
	  render_lisp_update_locally (child,
				      x + render_rect.x,
				      y + render_rect.y);
	  assert (child->rendered);
	  int child_height = child->rendered->h;
	  int child_width = child->rendered->w;
	  children_height_sum += child_height;
	  if (child_width > children_max_width)
	    children_max_width = child_width;
	  no_children++;
	  render_rect.y += RENDER_PADDING + child->rendered->h;				      
	}
      int render_width = 2 * RENDER_PADDING + children_max_width;
      int render_height = 2 * RENDER_PADDING + RENDER_PADDING * (no_children - 1) + children_height_sum;
      value->rendered = SDL_CreateRGBSurface (0, render_width,
					      render_height, 32,
					      0xff000000,
					      0x00ff0000,
					      0x0000ff00,
					      0x000000ff);
      assert (value->rendered);
      SDL_Rect canvas_rect;
      canvas_rect.x = 0;
      canvas_rect.y = 0;
      canvas_rect.w = render_width;
      canvas_rect.h = render_height;
      SDL_FillRect (value->rendered, &canvas_rect,
		    SDL_MapRGBA (value->rendered->format,
				0, 0, 0, 50));
      render_rect.x = RENDER_PADDING;
      render_rect.y = RENDER_PADDING;
      for (struct lisp_value *child = value->list.head;
	   child;
	   child = child->next)
	{
	  SDL_BlitSurface (child->rendered, NULL,
			   value->rendered, &render_rect);
	  render_rect.y += RENDER_PADDING + child->rendered->h;
	}
	   
    }
  else
    {
      char* stringz = malloc (value->atom.length + 1);
      memcpy (stringz, value->atom.chars, value->atom.length);
      stringz[value->atom.length] = 0;
      SDL_Color color = {255, 255, 255};
      value->rendered = TTF_RenderUTF8_Blended (font, stringz, color);
      SDL_SetSurfaceBlendMode (value->rendered, SDL_BLENDMODE_BLEND);
      SDL_SaveBMP (value->rendered, "out.bmp");
      free (stringz);
    }
}

