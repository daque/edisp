#pragma once

#include "sexp.h"

struct sexp* sexp_reader_read_file (char *file_name);
struct sexp* sexp_reader_read_string (char *string);
