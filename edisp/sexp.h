#pragma once

#include <SDL2/SDL.h>

#include <stdlib.h>

enum sexp_type
  {
    TYPE_LIST,
    TYPE_ATOM
  };

struct sexp_atom
{
  char *chars;
  size_t capacity;
  size_t length;
};

struct sexp_list
{
  struct sexp *head, *back;
};

struct sexp
{
  enum sexp_type type;
  struct sexp *parent, *next, *previous;
  union
  {
    struct sexp_atom atom;
    struct sexp_list list;
  };
};

struct sexp* sexp_new_atom ();
struct sexp* sexp_new_atom_from_stringz (const char *stringz);
struct sexp* sexp_new_list ();
void sexp_destroy (struct sexp* value);
void sexp_atom_insert_char (struct sexp* atom, char ch);
void sexp_list_push_back (struct sexp* list, struct sexp* new_back);
void sexp_list_push_front (struct sexp* list, struct sexp* new_front);
void sexp_print (struct sexp* value);
