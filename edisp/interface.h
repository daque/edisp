#pragma once

typedef void *edisp_file_t;
typedef void *edisp_sexp_t;
enum edisp_sexp_type
  {
    EDISP_SEXP_ATOM,
    EDISP_SEXP_LIST,
  };

edisp_file_t
edisp_read_file (char *file_name);
char *
edisp_file_get_file_name (edisp_file_t file);
edisp_sexp_t
edisp_file_get_sexp (edisp_file_t file);
enum edisp_sexp_type
edisp_sexp_get_type (edisp_sexp_t sexp);
char*
edisp_atom_get_string (edisp_sexp_t atom);
edisp_sexp_t
edisp_list_get_front (edisp_sexp_t list);
edisp_sexp_t
edisp_list_get_back (edisp_sexp_t list);
edisp_sexp_t
edisp_sexp_get_parent (edisp_sexp_t sexp);
edisp_sexp_t
edisp_sexp_get_next (edisp_sexp_t sexp);
edisp_sexp_t
edisp_sexp_get_previous (edisp_sexp_t sexp);
void
edisp_sexp_delete (edisp_sexp_t sexp);
edisp_sexp_t
edisp_new_list ();
edisp_sexp_t
edisp_new_atom (char *atom_string);
void
edisp_sexp_insert_next (edisp_sexp_t sexp);
void
edisp_sexp_insert_previous (edisp_sexp_t sexp);
void
edisp_push_front (edisp_sexp_t list, edisp_sexp_t front);
edisp_sexp_t
edisp_pop_front (edisp_sexp_t list);
void
edisp_push_back (edisp_sexp_t list, edisp_sexp_t back);
edisp_sexp_t
edisp_pop_back (edisp_sexp_t list);

