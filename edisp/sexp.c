#include "sexp.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>

struct sexp*
sexp_new_atom ()
{
  struct sexp* atom = malloc (sizeof (struct sexp));
  atom->type = TYPE_ATOM;
  atom->rendered = NULL;
  atom->parent = NULL;
  atom->next = NULL;
  atom->previous = NULL;
  atom->atom.chars = malloc (1);
  atom->atom.capacity = 1;
  atom->atom.length = 0;
  return atom;
}

struct sexp*
sexp_new_atom_from_stringz (const char *stringz)
{
  int stringz_length = strlen (stringz);
  struct sexp* atom = malloc (sizeof (struct sexp));
  atom->type = TYPE_ATOM;
  atom->rendered = NULL;
  atom->parent = NULL;
  atom->next = NULL;
  atom->previous = NULL;
  atom->atom.chars = malloc (stringz_length + 1);
  atom->atom.capacity = stringz_length + 1;
  atom->atom.length = stringz_length;
  memcpy (atom->atom.chars, stringz, stringz_length);
  return atom; 
}

struct sexp*
sexp_new_list ()
{
  struct sexp* list = malloc (sizeof (struct sexp));
  list->type = TYPE_LIST;
  list->rendered = NULL;
  list->parent = NULL;
  list->next = NULL;
  list->previous = NULL;
  list->list.head = NULL;
  list->list.back = NULL;
  return list;
}

void
sexp_destroy (struct sexp* value)
{
  if (value->type == TYPE_ATOM)
    {
      free (value->atom.chars);
      free (value);
      return;
    }
  for (struct sexp *it = value->list.head;
       it;
       it = it->next)
    lisp_destroy_value (it);
  free (value);
  return;
}

void
sexp_atom_insert_char (struct sexp *atom, char ch)
{
  if (atom->atom.length + 1 > atom->atom.capacity)
    {
      atom->atom.capacity *= 2;
      atom->atom.chars = realloc (atom->atom.chars,
				  atom->atom.capacity);
    }
  assert (atom->atom.length + 1 <= atom->atom.capacity);
  atom->atom.length++;
  atom->atom.chars[atom->atom.length - 1] = ch;
}

void
sexp_list_push_back (struct sexp* list, struct sexp* element)
{
  assert (element);
  element->parent = list;
  if (list->list.head == NULL)
    {
      list->list.head = element;
      list->list.back = element;
      return;
    }
  list->list.back->next = element;
  element->previous = list->list.back;
  list->list.back = element;
}

void
sexp_list_push_front (struct sexp* list, struct sexp* element)
{
  assert (element);
  element->parent = list;
  if (list->list.head == NULL)
    {
      list->list.head = element;
      list->list.back = element;
      return;
    }
  list->list.front->previous = element;
  element->next = list->list.front;
  list->list.front = element;
}

#include <stdio.h>

static void
print_indentation (int depth)
{
  for (int i = 0; i < depth; i++)
    {
      putchar (' ');
      putchar (' ');
    }
}

static void
print_auxiliar (struct sexp* value, int indented, int depth)
{
  if (!indented) print_indentation (depth);
  if (value->type == TYPE_ATOM)
    {
      for (int i = 0; i < value->atom.length; i++)
	{
	  putchar (value->atom.chars[i]);
	}
    }
  else
    {
      putchar ('(');
      for (struct sexp *it = value->list.head;
	   it;
	   it = it->next)
	{
	  if (it == value->list.head)
	    {
	      print_auxiliar (it, 1, depth + 1);
	    }
	  else
	    {
	      putchar ('\n');
	      print_auxiliar (it, 0, depth + 1);
	    }
	}
      putchar (')');
    }
}

void
sexp_print (struct sexp* value)
{
  print_auxiliar (value, 0, 0);
}
