#include "affine_transform.h"

struct affine_transform linear_interpolation (float a, float b, float A, float B)
{
  float len_dst = A - B;
  float len_src = a - b;
  struct affine_transform t;
  t.factor = len_dst / len_src;
  t.adder = A - a * t.factor;
  return t;
}

struct affine_transform_2d linear_interpolation_2d (struct float_rectangle a, struct float_rectangle b)
{
  struct affine_transform_2d t;
  t.t[0] = linear_interpolation (a.lo[0], a.hi[0], b.lo[0], b.hi[0]);
  t.t[1] = linear_interpolation (a.lo[1], a.hi[1], b.lo[1], b.hi[1]);
  return t;
}

void apply_affine_transform (struct affine_transform t, float *x)
{
  *x = t.factor * *x + t.adder;
}
void apply_affine_transform_2d (struct affine_transform_2d t, float *x, float *y)
{
  apply_affine_transform (t.t[0], x);
  apply_affine_transform (t.t[1], y);
}
