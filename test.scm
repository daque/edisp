(define (sqrt x)
  (expt x 0.5))
(define x 12)
(define root (floor (sqrt x)))
(if (= x (* root root))
    (progn
     (display x)
     (display " is a \"perfect square\"")
     (newline))
    (progn
     (display x)
     (display " is not a perfect square")
     (newline)))
(equation "x^2 + y^2 = z^2")
