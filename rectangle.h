#pragma once

struct float_rectangle
{
  float lo[2], hi[2];
};

struct int_rectangle
{
  int lo[2], hi[2];
};

struct int_rectangle int_rectangle_intersection (struct int_rectangle, struct int_rectangle);
struct float_rectangle float_rectangle_intersection (struct float_rectangle, struct float_rectangle);
float float_rectangle_get_size (struct float_rectangle r, int dimension);
int int_rectangle_get_size (struct int_rectangle r, int dimension);


