#pragma once

#define DECLARE_DYNAMIC_VECTOR(struct_name, base_type)			\
  struct struct_name							\
  {									\
    size_t capacity;							\
    size_t size;							\
    base_type *base_pointer;						\
  };									\
									\
  static inline void							\
  struct_name##_insert (struct struct_name *vector, base_type value)	\
  {									\
    assert (vector->capacity > 0);					\
    vector->size++;							\
    if (vector->capacity < vector->size)				\
      {									\
	vector->capacity *= 2;						\
	vector->base_pointer = (realloc (vector->base_pointer,		\
					 sizeof(base_type) * vector->capacity)); \
      }									\
    vector->base_pointer[vector->size-1] = value;			\
  }									\
									\
  static inline base_type*						\
  struct_name##_at (struct struct_name vector, int i)			\
  {									\
    return &vector.base_pointer[i];					\
  }									\
									\
  static inline struct struct_name					\
  struct_name##_new ()							\
  {									\
    struct struct_name vector;						\
    vector.capacity = 1;						\
    vector.size = 0;							\
    vector.base_pointer = malloc (sizeof(base_type));			\
    return vector;							\
  }									\
									\
  static inline void							\
  struct_name##_require_index (struct struct_name *vector, int index, base_type default_value) \
  {									\
    if (index < vector->size) return;					\
    int incremented = 0;						\
    while (index >= vector->capacity) vector->capacity *= 2, incremented = 1; \
    if (incremented)							\
      vector->base_pointer = (realloc (vector->base_pointer,		\
				       sizeof(base_type) * vector->capacity)); \
    for (int i = vector->size; i < vector->capacity; i++)		\
      vector->base_pointer[i] = default_value;				\
    vector->size = index+1;						\
  }									\
									\
  static inline base_type*						\
  struct_name##_force_at (struct struct_name *vector, int index, base_type default_value) \
  {									\
    struct_name##_require_index (vector, index, default_value);		\
    return &vector->base_pointer[index];				\
  }

#define DYNAMIC_VECTOR_FOREACH(v, vi, i, body) { int i = 0; for (typeof(v.base_pointer) vi = v.base_pointer; vi - v.base_pointer < v.size; vi++, i++) { body; } }

#define DYNAMIC_VECTOR_ELEMENT_INDEX(v, vi) ((vi) - (v).base_pointer)
