#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "dynamic_vector.h"

DECLARE_DYNAMIC_VECTOR(string, char);

int
main ()
{
  struct string str = string_new ();
  for (int i = 0; i < 10; i++)
    {
      string_insert (&str, 'a' + (i & 1));
    }
  DYNAMIC_VECTOR_FOREACH(str, ch, i, ({}));
  putchar ('\n');
  printf ("%*s\n", str.size, str.base_pointer);
  return 0;
}
