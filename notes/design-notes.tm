<TeXmacs|2.1.1>

<style|llncs>

<\body>
  <doc-data|<doc-title|Edisp Design Notes>|<doc-author|<author-data|<author-name|Miguel
  �ngel Mart�nez Quevedo>>>>

  <section|Motivation>

  Editing programs is one of the main activities in any kind of software
  development, and so, being this important, there have been a lot of
  development on how to make this editing process more eficient. Some of them
  have become quite good, but personally, I don't feel satisfied with them.\ 

  My hypothesis is that there are some fundamental assumptions about program
  editing and software in general that are holding back this development. I
  think that the core issue lies in the assumption that programs are
  fundamentally text. I don't think this is true.

  <subsection|What are Programs?>

  Most people think of text when they think of programs, but I think that
  this conception is misleading. I have thought about this for some time and
  I have concluded that: <with|font-series|bold|Programs are data
  structures>, and the way we represent them in text it's just that: a
  representation. Once someone thinks about this, I don't believe this
  assertion is really that controversial. If you take C code as an example is
  easy to understand what this means: A C file is a container of
  declarations, declarations can be of many types, but mainly variable or
  function declarations. Variable declarations are themselves containers of a
  type, an identifier, and optionally an initializer. Function declarations
  are containers of a return type, an identifier, a list of arguments, and a
  list of statements that compose the function. Statements can be of of many
  types, <text-dots> And so on. As I have said, maybe this statement by
  itself isn't that controversial or new but thinking enfatically on this
  makes you understand that the text representation is not somehow
  fundamentally attached to the program it represents, it's not
  <with|font-series|bold|necessary>. It is actually kind of
  <with|font-series|bold|accidental> that we have had to represent programs
  in text.\ 

  And so, if we don't represent programs in text, what other way can we
  represent them and why would we do that? It's obvious that when we store
  programs on disks, we have to somehow represent them in a plain format,
  composed of a string of bytes, and so their representation in disk doesn't
  necessarily have to change. But what can change is how they are represented
  when seen and changed. And I think that thinking about them in their tree
  form is much more useful than the plain representation we usually use. As I
  tried to illustrate in the last paragraph, programs can be thought of data
  structures composed of containers and atomic elements that aren't composed
  by anything else. Trees can very naturally represent this kind of
  relationships: Parents are containers and leaves are atomic elements. And
  so, for example, the C program:

  <\cpp-code>
    int x = 10;

    int a_function (int y)

    {

    \ \ return y;

    }
  </cpp-code>

  Can be thought of as the following tree:

  <\equation*>
    <tree|<text|c-program>|<tree|<text|variable-declaration>|<tree|<text|type>|<text|int>>|<tree|<text|name>|<text|x>>|<tree|<text|initializer>|10>>|<tree|<text|function-declaration>|<tree|<text|return-type>|int>|<tree|<text|name>|<text|a_function>>|<tree|<text|arguments>|<tree|<text|argument>|<tree|<text|type>|<text|int>>|<tree|<text|name>|<text|y>>>>|<tree|<text|body>|<tree|<text|return>|<text|y>>>>>
  </equation*>

  When we write programs, we are actually trying to convey these data
  structures in our code. In C in particular we try to make the compiler
  extract this data structure from our text. It is then through this more
  structural representation of programs that a compiler can operate more
  naturally on them, doing things like semantic analysis for example, or
  doing the translation per-se into machine-code. This represents in a way a
  level of indirection between ourselves and the syntactic structure we are
  trying to represent:

  <with|gr-mode|<tuple|edit|line>|gr-frame|<tuple|scale|1cm|<tuple|0.500001gw|0.5gh>>|gr-geometry|<tuple|geometry|0.918033par|0.6par|center>|gr-arrow-end|\<gtr\>|<graphics||<carc|<point|-4.02568|2.08083>|<point|-3.22134210874454|2.65233496494245>|<point|-3.56001124487366|1.86916258764387>>|<cline|<point|-4.02568|2.08083>|<point|-4.30084998015611|0.704987432200026>|<point|-2.88267297261543|0.726154253208096>|<point|-3.20360884511268|2.09163118087856>>|<text-at|Programmer|<point|-4.55485183225294|0.0699828019579309>>|<with|arrow-end|\<gtr\>|<line|<point|-2.417|1.50933>|<point|-1.57033007011509|1.50932663050668>>>|<\document-at>
    \;
  </document-at|<point|-4.80885|2.1655>>|<\document-at>
    <\cpp-code>
      <\with|font-base-size|6>
        int x = 10;

        int a_f<with|font-base-size|6|>unction (int y)

        {

        \ \ return y;

        }
      </with>
    </cpp-code>
  </document-at|<point|-1.40099550205054|2.22899854478106>>|<\document-at>
    \;
  </document-at|<point|2.3667|2.33483>>|<\document-at>
    <\equation*>
      <with|font-base-size|6|<tree|<text|c-program>|<tree|<text|variable-declaration>|<tree|<text|type>|<text|int>>|<tree|<text|name>|<text|x>>|<tree|<text|initializer>|10>>|<tree|<text|function-declaration>|<tree|<text|return-type>|int>|<tree|<text|name>|<text|a_function>>|<tree|<text|arguments>|<tree|<text|argument>|<tree|<text|type>|<text|int>>|<tree|<text|name>|<text|y>>>>|<tree|<text|body>|<tree|<text|return>|<text|y>>>>>>
    </equation*>
  </document-at|<point|-2.92648936360174|-0.116169466860696>>|<with|arrow-end|\<gtr\>|<line|<point|1.01202|1.10716>|<point|1.03212689444003|0.00138907262865458>>>>>

  This may seem like a necessary thing, and there may seem to be almost no
  motivation to change it, but as I have stated from the beginning: I don't
  think this is necessary and I think this notion of it being necessary is
  holding back the tools we use to edit code. How? This level of indirection
  creates a necessity for complicated tooling to provide editing capabilites
  that require the editor to understand the syntactic structures being
  edited. The fact that it's not trivial for example for an editor to know
  what is the \Pinner-most\Q control structure being edited, or being able to
  for example show what \Pif\Q statement a particular \P}\Q is closing. This
  indirection is also the reason there even are errors like missing \P;\Q in
  statements or other minor annoyances like that that just create unneeded
  friction.

  <section|Design>

  Edisp is fundamentally a syntactic tree editor. As can be easily seen from
  the example of C code represented as a tree, a plain representation can
  easily make the code be more mentally overwhelming, so it must be able to
  create more succint and easy to understand visual representations. The
  concrete way in which we are reading and thinking about this tree is
  through lisp expressions. And so the tree's nodes that are parents cannot
  have data such as a symbol directly associated with them, and order of
  children are important. So we can't directly represent:

  <\equation*>
    <tree|<text|c-program>|<tree|<text|variable-declaration>|<tree|<text|type>|<text|int>>|<tree|<text|name>|<text|x>>|<tree|<text|initializer>|10>>|<tree|<text|function-declaration>|<tree|<text|return-type>|int>|<tree|<text|name>|<text|a_function>>|<tree|<text|arguments>|<tree|<text|argument>|<tree|<text|type>|<text|int>>|<tree|<text|name>|<text|y>>>>|<tree|<text|body>|<tree|<text|return>|<text|y>>>>>
  </equation*>

  And instead we do something like this:

  <\equation*>
    <tree|\<frak-L\>|<tree|\<frak-L\>|var.|<text|int>|<text|x>|10>|<tree|\<frak-L\>|<text|func.>|<text|int>|<text|a_function>|<tree|\<frak-L\>|<tree|\<frak-L\>|<text|int>|<text|x>>>|<tree|\<frak-L\>|<tree|\<frak-L\>|<text|return>|<text|x>>>>>
  </equation*>

  Using the order property to our convenience, we can make the direct textual
  tree representation shown here a little bit simpler.

  The example shown then corresponds to the lisp expression:

  <\scm-code>
    ((var int x 10)

    \ (func int a_function ((int x)) ((return x))))
  </scm-code>

  For the moment, this is the text file that we interpret and the
  corresponding tree representation that we as a program manipulate and
  represent visually for the user.

  The main problem we face is the visual representation problem. As the ways
  in which the user can manipulate the syntactic tree and thus the usefulness
  of Edisp lies primarily in this representation.

  <subsection|Some initial design decisions.>

  Through much consideration I have determined that the best course of action
  to investigate this issue is to produce first an example application and
  then build most of the design from that.

  Despite of this, I recognize that even when doing first a working prototype
  I require to do some fundamental design decisions. I have already devised
  some of the main components on the architecture and the general philosophy
  behind Edisp. The most important decision that I have taken in this regard,
  is that Edisp won't be a single program but rather firstly a framework in
  which building structure editors will be supported. In this direction,
  <with|font-series|bold|edisp> will provide services to
  <with|font-series|bold|applications>. Edisp and applications will both have
  ways to extend their functionality through <with|font-series|bold|addons>.
  I have foreseen but not decided that edisp addons will work only through
  edisp-wide interfaces while application addons can have many shapes and
  forms, and will be able to use both standard edisp-wide functionality and
  non-standard application-specific functionality (or something in between,
  maybe).

  As Edisp will provide services, it is mandatory that we define which kind
  of services and specifically list the available interfaces that it will
  implement.

  Edisp will in general provide data reading/writing, and data
  interpretation/serialization.\ 

  The main forms of the data reading/writing mechanism will obviously be by
  file reading/writing but it shouldn't be restricted to only that kind of
  data as it should be possible to even interpret data coming from the
  network, or data coming from the directory structure of the filesystem,
  etc.\ 

  The data interpretation/serialization consists of the mapping between the
  raw data to s-expressions and it's inverse.

  <\wide-tabular>
    <tformat|<twith|table-lborder|10>|<twith|table-rborder|10>|<twith|table-bborder|10>|<twith|table-tborder|10>|<cwith|1|-1|1|-1|cell-tborder|1ln>|<cwith|1|-1|1|-1|cell-bborder|1ln>|<cwith|1|-1|1|-1|cell-lborder|1ln>|<cwith|1|-1|1|-1|cell-rborder|1ln>|<twith|table-hyphen|y>|<table|<row|<\cell>
      read-file (file-name): file
    </cell>|<\cell>
      Reads the specified file from disk, interprets it as a sexp and returns
      a handle to the file-tree pair.
    </cell>>|<row|<\cell>
      file-get-file-name (file-handle): file-name
    </cell>|<\cell>
      Gets the file-name from the specified file-handle.
    </cell>>|<\row|<\cell>
      file-get-sexp (file-handle): sexp
    </cell>>
      gets the root sexp node representing the file identified by the given
      handle.
    </row>|<row|<\cell>
      sexp-get-type(sexp-node): sexp-note-type
    </cell>|<\cell>
      Gets the type of the specified sexp-node, a sexp node can be \Patom\Q
      and \Plist\Q.
    </cell>>|<row|<\cell>
      atom-get-string(sexp-node): atom-string
    </cell>|<\cell>
      Gets as a string the atomic value held in the sexp-node. The given
      sexp-node must be of type atom.
    </cell>>|<row|<\cell>
      list-get-front(sexp-node): sexp-node
    </cell>|<\cell>
      Gets the head of the list defined in sexp-node. The given sexp-node
      must be of type list.
    </cell>>|<row|<\cell>
      list-get-back(sexp-node): sexp-node
    </cell>|<\cell>
      Gets the back of the list defined in sexp-node. The given sexp-node
      must be of type list.
    </cell>>|<row|<\cell>
      sexp-delete (sexp-node): nothing
    </cell>|<\cell>
      Deletes the given sexp node from it's tree, updating the required
      references in his siblings (if it has any) and possibly also it's
      parent (if the deleted note was one of it's parent's ends). Frees
      associated memory as well as recursively deleting it's children if the
      given node is a list.
    </cell>>|<\row>
      new-list (): sexp
    </row|<\cell>
      Allocates and initializes an empty list. The returned node is an
      orphan.
    </cell>>|<row|<\cell>
      new-atom (atom-string): sexp
    </cell>|<\cell>
      Allocates and initializes an atom given the string that it will hold.
      The returned node is an orphan.
    </cell>>|<row|<\cell>
      sexp-insert-next (sexp into, sexp new): void
    </cell>|<\cell>
      Inserts the given sexp in \Pnew\Q as the next sexp of the node
      \Pinto\Q. If \Pinto\Q already had a \Pnext\Q node, references are
      updated so that the previous reference of the original next node refers
      to the new node, while the next reference of the into node refers to
      the new node, effectively inserting the new node between into and it's
      original next node. This function also updates into's parent back
      reference if into was it's parent back. The new node may or not be
      originally an orphan, but if it is, it doesn't remain so after the
      operation.
    </cell>>|<row|<\cell>
      sexp-insert-previous (sexp into, sexp new): nothing
    </cell>|<\cell>
      mirror of sexp-insert-next
    </cell>>|<row|<\cell>
      push-front (list, new-front): nothing\ 
    </cell>|<\cell>
      Push a new front node into the given list.
    </cell>>|<row|<\cell>
      pop-front (list): sexp
    </cell>|<\cell>
      Pops and returns the front node of the given list. The node is returned
      as an orphan.
    </cell>>|<row|<\cell>
      push-back (list, new-back): nothing
    </cell>|<\cell>
      Push a new back node into the given list.
    </cell>>|<row|<\cell>
      pop-back (list): sexp
    </cell>|<\cell>
      Pops and returns the back node of the given list. The node is returned
      as an orphan.
    </cell>>|<row|<\cell>
      \;
    </cell>|<\cell>
      \;
    </cell>>>>
  </wide-tabular>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|1.1|1>>
    <associate|auto-3|<tuple|2|3>>
    <associate|auto-4|<tuple|2.1|4>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Motivation>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>What are Programs?
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Design>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Some initial design
      decisions. <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>